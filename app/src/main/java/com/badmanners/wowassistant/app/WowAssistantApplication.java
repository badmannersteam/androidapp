package com.badmanners.wowassistant.app;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.badmanners.wowassistant.helpers.HelperFactory;
import com.koushikdutta.ion.Ion;

/**
 * Created by bigma on 27.09.2016.
 */

public class WowAssistantApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        HelperFactory.setHelpers(this);
    }

    public static WowAssistantApplication get(Context context){
        return (WowAssistantApplication) context.getApplicationContext();
    }

}
