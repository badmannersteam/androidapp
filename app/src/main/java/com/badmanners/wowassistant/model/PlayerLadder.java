package com.badmanners.wowassistant.model;

import android.widget.ArrayAdapter;

import java.util.ArrayList;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bigma on 06.10.2016.
 */

public class PlayerLadder extends RealmObject {

    @PrimaryKey
    private Integer rank;
    private Integer rankRegion;
    private Integer rating;
    private Integer  regionId;
    private String name = "";
    private String realm = "";
    private Integer raceId;
    private Integer classId;
    private Integer specId;
    private Integer factionId;
    private Integer genderId;
    private Integer winSeason;
    private Integer loseSeason;
    private Integer winWeek;
    private Integer loseWeek;

    public PlayerLadder(){}

    public Integer getRank() {
        return rank;
    }

    public void setRank(Integer rank) {
        this.rank = rank;
    }

    public Integer getRankRegion() {
        return rankRegion;
    }

    public void setRankRegion(Integer rankRegion) {
        this.rankRegion = rankRegion;
    }

    public Integer getRegionId() {
        return regionId;
    }

    public void setRegionId(Integer regionId) {
        this.regionId = regionId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRealm() {
        return realm;
    }

    public void setRealm(String realm) {
        this.realm = realm;
    }

    public Integer getRaceId() {
        return raceId;
    }

    public void setRaceId(Integer raceId) {
        this.raceId = raceId;
    }

    public Integer getClassId() {
        return classId;
    }

    public void setClassId(Integer classId) {
        this.classId = classId;
    }

    public Integer getSpecId() {
        return specId;
    }

    public void setSpecId(Integer specId) {
        this.specId = specId;
    }

    public Integer getFactionId() {
        return factionId;
    }

    public void setFactionId(Integer factionId) {
        this.factionId = factionId;
    }

    public Integer getGenderId() {
        return genderId;
    }

    public void setGenderId(Integer genderId) {
        this.genderId = genderId;
    }

    public Integer getWinSeason() {
        return winSeason;
    }

    public void setWinSeason(Integer winSeason) {
        this.winSeason = winSeason;
    }

    public Integer getLoseSeason() {
        return loseSeason;
    }

    public void setLoseSeason(Integer loseSeason) {
        this.loseSeason = loseSeason;
    }

    public Integer getWinWeek() {
        return winWeek;
    }

    public void setWinWeek(Integer winWeek) {
        this.winWeek = winWeek;
    }

    public Integer getLoseWeek() {
        return loseWeek;
    }

    public void setLoseWeek(Integer loseWeek) {
        this.loseWeek = loseWeek;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
