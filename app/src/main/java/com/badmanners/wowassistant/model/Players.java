package com.badmanners.wowassistant.model;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bigma on 07.10.2016.
 */

public class Players extends RealmObject {
    public static final int LADDER_3V3 = 1;
    public static final int LADDER_2V2 = 2;
    public static final int LADDER_RBG = 3;

    @PrimaryKey
    private int ladderId;
    private int page;
    private RealmList<PlayerLadder> players;

    public Players(){}

    public Players(int ladderId){
        this.ladderId=ladderId;
    }

    public RealmList<PlayerLadder> getPlayers() {
        return players;
    }

    public void setPlayers(RealmList<PlayerLadder> players) {
        this.players = players;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLadderId() {
        return ladderId;
    }

    public void setLadderId(int ladderId) {
        this.ladderId = ladderId;
    }


}
