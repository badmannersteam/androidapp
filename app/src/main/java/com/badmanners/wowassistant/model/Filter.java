package com.badmanners.wowassistant.model;

import com.google.gson.JsonObject;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by bigma on 09.10.2016.
 */

public class Filter extends RealmObject {
    @PrimaryKey
    private int ladderId;
    private String filterBody;

    public String getFilterBody() {
        return filterBody;
    }

    public void setFilterBody(String filterBody) {
        this.filterBody = filterBody;
    }

    public Filter() {
    }

    public Filter(int ladderId) {
        this.ladderId = ladderId;
    }


}
