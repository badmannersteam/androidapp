package com.badmanners.wowassistant.helpers;


import android.graphics.Color;

import com.badmanners.wowassistant.R;

/**
 * Created by bigma on 03.10.2016.
 */

public class Chooser {

    public static int getClassIcon(int classId){
        int icon=-1;
        switch (classId){
            case 1:
                icon = R.drawable.warrior;
                break;
            case 2:
                icon = R.drawable.paladin;
                break;
            case 3:
                icon = R.drawable.hunter;
                break;
            case 4:
                icon = R.drawable.rogue;
                break;
            case 5:
                icon = R.drawable.priest;
                break;
            case 6:
                icon = R.drawable.deathknight;
                break;
            case 7:
                icon = R.drawable.shaman;
                break;
            case 8:
                icon = R.drawable.mage;
                break;
            case 9:
                icon = R.drawable.warlock;
                break;
            case 10:
                icon = R.drawable.monk;
                break;
            case 11:
                icon = R.drawable.druid;
                break;
            case 12:
                icon = R.drawable.demonhunter;
                break;
            default:
                break;
        }
        return icon;
    }

    public static int getClassColor(int classId){
        int color;
        switch (classId){
            case 1:
                color = R.color.warrior;
                break;
            case 2:
                color = R.color.paladin;
                break;
            case 3:
                color = R.color.hunter;
                break;
            case 4:
                color = R.color.rogue;
                break;
            case 5:
                color = R.color.priest;
                break;
            case 6:
                color = R.color.deathknight;
                break;
            case 7:
                color = R.color.shaman;
                break;
            case 8:
                color = R.color.mage;
                break;
            case 9:
                color = R.color.warlock;
                break;
            case 10:
                color = R.color.monk;
                break;
            case 11:
                color = R.color.druid;
                break;
            case 12:
                color = R.color.demonhunter;
                break;
            default:
                color = -1;
                break;
        }
        return color;
    }

    public static int getRaceIcon(int raceId, int genderId){
        int icon=-1;
        if (genderId==0){
            switch (raceId){
                case 1:
                    icon = R.drawable.human_female;
                    break;
                case 2:
                    icon = R.drawable.orc_female;
                    break;
                case 3:
                    icon = R.drawable.dwarf_female;
                    break;
                case 4:
                    icon = R.drawable.nightelf_female;
                    break;
                case 5:
                    icon = R.drawable.scourge_female;
                    break;
                case 6:
                    icon = R.drawable.tauren_female;
                    break;
                case 7:
                    icon = R.drawable.gnome_female;
                    break;
                case 8:
                    icon = R.drawable.troll_female;
                    break;
                case 9:
                    icon = R.drawable.goblin_female;
                    break;
                case 10:
                    icon = R.drawable.bloodelf_female;
                    break;
                case 11:
                    icon = R.drawable.draenei_female;
                    break;
                case 22:
                    icon = R.drawable.worgen_female;
                    break;
                case 25:
                    icon = R.drawable.pandaren_female;
                    break;
                case 26:
                    icon = R.drawable.pandaren_female;
                    break;
            }
        } else {
            switch (raceId){
                case 1:
                    icon = R.drawable.human_male;
                    break;
                case 2:
                    icon = R.drawable.orc_male;
                    break;
                case 3:
                    icon = R.drawable.dwarf_male;
                    break;
                case 4:
                    icon = R.drawable.nightelf_male;
                    break;
                case 5:
                    icon = R.drawable.scourge_male;
                    break;
                case 6:
                    icon = R.drawable.tauren_male;
                    break;
                case 7:
                    icon = R.drawable.gnome_male;
                    break;
                case 8:
                    icon = R.drawable.troll_male;
                    break;
                case 9:
                    icon = R.drawable.goblin_male;
                    break;
                case 10:
                    icon = R.drawable.bloodelf_male;
                    break;
                case 11:
                    icon = R.drawable.draenei_male;
                    break;
                case 22:
                    icon = R.drawable.worgen_male;
                    break;
                case 25:
                    icon = R.drawable.pandaren_male;
                    break;
                case 26:
                    icon = R.drawable.pandaren_male;
                    break;
            }
        }
        return icon;
    }

    public static int getFactionColor(int factionId){
        int color;
        switch (factionId){
            case 0:
                color = R.color.blue;
                break;
            case 1:
                color = R.color.red;
                break;
            default:
                color = -1;
                break;
        }
        return color;
    }

    public static int getRegionIcon(int regionId){
        int icon = -1;
        switch (regionId){
            case 1:
                icon = R.drawable.region_eu;
                break;
            case 2:
                icon = R.drawable.region_us;
                break;
        }
        return icon;
    }

    public static int getSpecIcon(int specId){
        int icon = -1;
        switch (specId){
            case 250:
                icon = R.drawable.blood;
                break;
            case 251:
                icon = R.drawable.frost;
                break;
            case 252:
                icon = R.drawable.unholy;
                break;
            case 577:
                icon = R.drawable.dps;
                break;
            case 581:
                icon = R.drawable.tank;
                break;
            case 102:
                icon = R.drawable.balance;
                break;
            case 103:
                icon = R.drawable.feral;
                break;
            case 104:
                icon = R.drawable.guardian;
                break;
            case 105:
                icon = R.drawable.druid_restoration;
                break;
            case 253:
                icon = R.drawable.beastmastery;
                break;
            case 254:
                icon = R.drawable.marksman;
                break;
            case 255:
                icon = R.drawable.survival;
                break;
            case 62:
                icon = R.drawable.arcane;
                break;
            case 63:
                icon = R.drawable.fire;
                break;
            case 64:
                icon = R.drawable.mage_frost;
                break;
            case 268:
                icon = R.drawable.brewmaster;
                break;
            case 269:
                icon = R.drawable.windwalker;
                break;
            case 270:
                icon = R.drawable.mistweaver;
                break;
            case 65:
                icon = R.drawable.holy;
                break;
            case 66:
                icon = R.drawable.paladin_protection;
                break;
            case 70:
                icon = R.drawable.retribution;
                break;
            case 256:
                icon = R.drawable.discipline;
                break;
            case 257:
                icon = R.drawable.priest_holy;
                break;
            case 258:
                icon = R.drawable.shadow;
                break;
            case 259:
                icon = R.drawable.assassination;
                break;
            case 260:
                icon = R.drawable.outlaw;
                break;
            case 261:
                icon = R.drawable.subtlety;
                break;
            case 262:
                icon = R.drawable.elemental;
                break;
            case 263:
                icon = R.drawable.enhancement;
                break;
            case 264:
                icon = R.drawable.restoration;
                break;
            case 265:
                icon = R.drawable.affliction;
                break;
            case 266:
                icon = R.drawable.demonology;
                break;
            case 267:
                icon = R.drawable.destruction;
                break;
            case 71:
                icon = R.drawable.arms;
                break;
            case 72:
                icon = R.drawable.fury;
                break;
            case 73:
                icon = R.drawable.protection;
                break;
        }
        return icon;
    }
}