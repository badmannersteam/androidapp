package com.badmanners.wowassistant.helpers;

import android.content.Context;

import com.google.gson.JsonArray;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import com.google.gson.JsonObject;

/**
 * Created by bigma on 30.09.2016.
 */

public class RequestBuilder {


    private Context applicationContext;
    private boolean mUsed=false;
    private String url;
    private FutureCallback<JsonObject> callback;


    public RequestBuilder(Context context){
        this.applicationContext=context;
    }

    public RequestBuilder withCallback(FutureCallback<JsonObject> callback){
        this.callback=callback;
        return this;
    }


    public RequestBuilder withURL(String url){
        this.url=url;
        return this;
    }



    public Request build(){
        if (mUsed) {
            throw new RuntimeException("you must not reuse a RequestBuilder builder");
        }
        if (applicationContext == null) {
            throw new RuntimeException("Please pass the context");
        }

        mUsed = true;
        return new Request(applicationContext,url,callback);
    }
}
