package com.badmanners.wowassistant.helpers;

import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.stream.JsonReader;
import com.koushikdutta.async.future.Future;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.io.IOException;
import java.io.Reader;
import java.util.Arrays;


/**
 * Created by bigma on 30.09.2016.
 */

public class Request
{

    private Context applicationContext;
    private String url;
    private FutureCallback<JsonObject> callback;
    private Future<JsonObject> result;

    protected Request(@NonNull Context applicationContext,@NonNull String url,@NonNull FutureCallback<JsonObject> callback) {
        this.applicationContext = applicationContext;
        this.url = url;
        this.callback = callback;
    }

    public void send(){
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("page",0);
        jsonObject.addProperty("pageSize",50);
        jsonObject.addProperty("minRating",2000);
        jsonObject.addProperty("maxRating",2500);
        jsonObject.addProperty("ratingFilter",true);
        jsonObject.add("classId",new JsonParser().parse(Arrays.toString(new int[]{1,3,7})));
        jsonObject.addProperty("classFilter",true);
        result = Ion.with(applicationContext)
                .load(url)
                .setJsonObjectBody(jsonObject)
                .asJsonObject()
                .setCallback(callback);
    }

    public void cancel(){
        if(result!=null)
            result.cancel();
        result=null;
    }
}
