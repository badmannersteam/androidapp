package com.badmanners.wowassistant.helpers;

import android.content.Context;
import android.util.Log;

import com.koushikdutta.ion.Ion;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by bigma on 06.10.2016.
 */


public class HelperFactory {
    private static Realm mRealm;

    public static void setHelpers(Context context) {

        // Create configuration
        RealmConfiguration realmConfig = new RealmConfiguration.Builder(context)
                .name("wow_assistant.realm")
                .deleteRealmIfMigrationNeeded()
                .build();
        // Set default configuration
        Realm.setDefaultConfiguration(realmConfig);

        Ion.getDefault(context)
                .configure().setLogging("REQUEST", Log.DEBUG);
    }

    public static void releaseHelpers() {
        mRealm.close();
        mRealm = null;
    }

    public static Realm getDatabase() {
        return Realm.getDefaultInstance(); // return default instance of Realm
    }
}
