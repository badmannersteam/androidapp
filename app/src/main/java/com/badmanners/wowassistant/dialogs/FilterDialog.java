package com.badmanners.wowassistant.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;
import android.widget.FrameLayout;
import android.widget.GridView;

import com.badmanners.wowassistant.R;
import com.badmanners.wowassistant.customviews.CheckableCircleImageView;

import de.hdodenhof.circleimageview.CircleImageView;


/**
 * Created by bigma on 05.10.2016.
 */

public class FilterDialog extends DialogFragment {
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_filter, null, false);
        /*final CheckableCircleImageView circleImageView = (CheckableCircleImageView) view.findViewById(R.id.test);
        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(circleImageView.isSelected())
                    circleImageView.setSelected(false);
                else circleImageView.setSelected(true);
            }
        });*/
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(view);
        builder.setCancelable(false);
        builder.setPositiveButton(R.string.action_about, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dismiss();
            }
        });
        return builder.create();
    }
}
