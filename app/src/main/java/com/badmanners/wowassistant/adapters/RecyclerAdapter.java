package com.badmanners.wowassistant.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.badmanners.wowassistant.helpers.Chooser;
import com.badmanners.wowassistant.R;
import com.badmanners.wowassistant.helpers.HelperFactory;
import com.badmanners.wowassistant.model.Players;
import com.google.gson.JsonObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * Created by bigma on 29.09.2016.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private Context parentContext;
    private int page;
    private Players players;

    //region Constructors
    public RecyclerAdapter(){
    }

    public RecyclerAdapter(int page){
        this.page = page;
    }

    public RecyclerAdapter(Players players) {
        this.players = players;
    }

    public RecyclerAdapter(int page, Players players) {
        this.page = page;
        this.players = players;
    }
    //endregion

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item,parent,false);
        parentContext=parent.getContext();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (players!=null || players.getPlayers().size()>0){
            holder.rankWorld.setText(players.getPlayers().get(position).getRank().toString());
            holder.rankRegion.setText(players.getPlayers().get(position).getRankRegion().toString());
            holder.rating.setText(players.getPlayers().get(position).getRating().toString());
            holder.name.setText(players.getPlayers().get(position).getName());
            holder.name.setTextColor(parentContext.getResources().getColor(Chooser.getClassColor(players.getPlayers().get(position).getClassId())));
            holder.realm.setText(players.getPlayers().get(position).getRealm());
            holder.realm.setTextColor(parentContext.getResources().getColor(Chooser.getFactionColor(players.getPlayers().get(position).getFactionId())));
            holder.winSeason.setText(players.getPlayers().get(position).getWinSeason().toString());
            holder.loseSeason.setText(players.getPlayers().get(position).getLoseSeason().toString());
            holder.winWeek.setText(players.getPlayers().get(position).getWinWeek().toString());
            holder.loseWeek.setText(players.getPlayers().get(position).getLoseWeek().toString());
            holder.regionIcon.setImageResource(Chooser.getRegionIcon(players.getPlayers().get(position).getRegionId()));
            holder.classIcon.setImageResource(Chooser.getClassIcon(players.getPlayers().get(position).getClassId()));
            holder.raceIcon.setImageResource(Chooser.getRaceIcon(players.getPlayers().get(position).getRaceId(),players.getPlayers().get(position).getGenderId()));
            holder.specIcon.setImageResource(Chooser.getSpecIcon(players.getPlayers().get(position).getSpecId()));
            holder.playedSeason.setText(String.valueOf(Integer.valueOf(holder.winSeason.getText().toString())+Integer.valueOf(holder.loseSeason.getText().toString())));
            holder.playedWeek.setText(String.valueOf(Integer.valueOf(holder.winWeek.getText().toString())+Integer.valueOf(holder.loseWeek.getText().toString())));
        }
    }

    @Override
    public int getItemCount() {
        if(players!=null)
            return players.getPlayers().size();
        else return 0;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.rank_world)
        TextView rankWorld;

        @BindView(R.id.rank_region)
        TextView rankRegion;

        @BindView(R.id.region_icon)
        ImageView regionIcon;

        @BindView(R.id.rating)
        TextView rating;

        @BindView(R.id.name)
        TextView name;

        @BindView(R.id.race_icon)
        CircleImageView raceIcon;

        @BindView(R.id.class_icon)
        CircleImageView classIcon;

        @BindView(R.id.spec_icon)
        CircleImageView specIcon;

        @BindView(R.id.realm)
        TextView realm;

        @BindView(R.id.played_week)
        TextView playedWeek;

        @BindView(R.id.win_week)
        TextView winWeek;

        @BindView(R.id.lose_week)
        TextView loseWeek;

        @BindView(R.id.played_season)
        TextView playedSeason;

        @BindView(R.id.win_season)
        TextView winSeason;

        @BindView(R.id.lose_season)
        TextView loseSeason;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }

}
