package com.badmanners.wowassistant.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.badmanners.wowassistant.R;
import com.badmanners.wowassistant.fragments.MainViewpagerFragment;
import com.badmanners.wowassistant.model.Players;

/**
 * Created by bigma on 08.10.2016.
 */

public class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {

    private Context context;
    private String tabTitles[];

    public ScreenSlidePagerAdapter(FragmentManager fm,Context context) {
        super(fm);
        this.context=context;
        tabTitles = new String[]{context.getString(R.string.ladder_3v3), context.getString(R.string.ladder_2v2), context.getString(R.string.ladder_rbg)};
    }

    @Override
    public Fragment getItem(int position) {
        MainViewpagerFragment fragment = new MainViewpagerFragment();
        switch (position){
            case 0:
                fragment.setLadderId(Players.LADDER_3V3);
                break;
            case 1:
                fragment.setLadderId(Players.LADDER_2V2);
                break;
            case 2:
                fragment.setLadderId(Players.LADDER_RBG);
                break;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}