package com.badmanners.wowassistant.customviews;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.support.annotation.NonNull;
import android.util.AttributeSet;

import com.badmanners.wowassistant.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by bigma on 05.10.2016.
 */

public class CheckableCircleImageView extends CircleImageView {

    private boolean selected = true;
    private Bitmap tickBmp;
    private Paint paint;
    private Paint mDarkerPaint;
    private int measuredWidth, measuredHeight;


    public CheckableCircleImageView(Context context) {
        super(context);
        init();
    }

    public CheckableCircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public CheckableCircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    public void setTickBmp(int id){
        tickBmp = BitmapFactory.decodeResource(getResources(), id);
    }


    private void init() {
        paint = new Paint();
        mDarkerPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mDarkerPaint.setStyle(Paint.Style.FILL);
        // Keep changing this color till it looks ok for you
        mDarkerPaint.setColor(0x80142030);
        tickBmp = BitmapFactory.decodeResource(getResources(), R.drawable.checkmark);
    }


    public void setSelected(boolean selected) {
        this.selected = selected;
        invalidate();
    }

    public boolean isSelected() {
        return selected;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        measuredHeight = getDefaultSize(getSuggestedMinimumHeight(), heightMeasureSpec);
        measuredWidth = getDefaultSize(getSuggestedMinimumWidth(), widthMeasureSpec);

        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    @Override
    protected void onDraw(@NonNull Canvas canvas) {
        super.onDraw(canvas);

        if (selected) {
            int margin = 0;
            int x = (canvas.getWidth() / 2) - (tickBmp.getWidth() / 2) - margin;
            int y = (canvas.getHeight() / 2) - (tickBmp.getHeight() / 2) - margin;

            canvas.drawCircle((canvas.getWidth() / 2), (canvas.getHeight() / 2), measuredWidth/2, mDarkerPaint);
            canvas.drawBitmap(tickBmp, x, y, paint);
        }
    }
}
