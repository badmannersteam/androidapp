package com.badmanners.wowassistant.fragments;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.badmanners.wowassistant.R;
import com.badmanners.wowassistant.activities.MainActivity;
import com.badmanners.wowassistant.adapters.RecyclerAdapter;
import com.badmanners.wowassistant.helpers.HelperFactory;
import com.badmanners.wowassistant.helpers.Request;
import com.badmanners.wowassistant.helpers.RequestBuilder;
import com.badmanners.wowassistant.model.PlayerLadder;
import com.badmanners.wowassistant.model.Players;
import com.google.gson.JsonObject;
import com.kennyc.view.MultiStateView;
import com.koushikdutta.async.future.FutureCallback;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmList;
import jp.wasabeef.recyclerview.adapters.AlphaInAnimationAdapter;

/**
 * Created by bigma on 08.10.2016.
 */

public class MainViewpagerFragment extends Fragment {

    @BindView(R.id.recycler)
    RecyclerView recyclerView;

    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout refreshLayout;

    @BindView(R.id.multistate_view)
    MultiStateView multiStateView;

    private RecyclerAdapter adapter;
    private Request request;

    private Realm realm = HelperFactory.getDatabase();
    private Players players;
    private int ladderId;

    @Override
    public void onStart() {
        super.onStart();
        realm.beginTransaction();
        players = realm.where(Players.class).equalTo("page", 0).equalTo("ladderId", ladderId).findFirst();
        realm.commitTransaction();
        if (players == null && request!=null) {
            request.send();
            refreshLayout.setRefreshing(true);
        } else if (players!=null){
            updateRecycler(players);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_viewpager, container, false);
        ButterKnife.bind(this, rootView);
        init();
        initListeners();
        return rootView;
    }


    public void init() {
        //region Views
        adapter = new RecyclerAdapter();
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(new AlphaInAnimationAdapter(adapter));
        multiStateView.setViewState(MultiStateView.VIEW_STATE_EMPTY);
        //endregion
    }

    public void initListeners() {
        //region Request callback
        FutureCallback<JsonObject> callback = new FutureCallback<JsonObject>() {
            @Override
            public void onCompleted(Exception e, JsonObject result) {
                refreshLayout.setRefreshing(false);
                if (result != null) {
                    players = new Players(ladderId);
                    players.setPlayers(new RealmList<PlayerLadder>());
                    players.setPage(0);
                    for (int i = 0; i < result.getAsJsonArray("players").size(); i++) {
                        PlayerLadder player = new PlayerLadder();
                        player.setClassId(result.getAsJsonArray("players").get(i).getAsJsonObject().get("classId").getAsInt());
                        player.setFactionId(result.getAsJsonArray("players").get(i).getAsJsonObject().get("factionId").getAsInt());
                        player.setGenderId(result.getAsJsonArray("players").get(i).getAsJsonObject().get("genderId").getAsInt());
                        player.setRank(result.getAsJsonArray("players").get(i).getAsJsonObject().get("totalRanking").getAsInt());
                        player.setRankRegion(result.getAsJsonArray("players").get(i).getAsJsonObject().get("ranking").getAsInt());
                        player.setRaceId(result.getAsJsonArray("players").get(i).getAsJsonObject().get("raceId").getAsInt());
                        player.setSpecId(result.getAsJsonArray("players").get(i).getAsJsonObject().get("specId").getAsInt());
                        player.setName(result.getAsJsonArray("players").get(i).getAsJsonObject().get("name").getAsString());
                        player.setRealm(result.getAsJsonArray("players").get(i).getAsJsonObject().get("realmName").getAsString());
                        player.setWinSeason(result.getAsJsonArray("players").get(i).getAsJsonObject().get("seasonWins").getAsInt());
                        player.setWinWeek(result.getAsJsonArray("players").get(i).getAsJsonObject().get("weeklyWins").getAsInt());
                        player.setLoseSeason(result.getAsJsonArray("players").get(i).getAsJsonObject().get("seasonLosses").getAsInt());
                        player.setLoseWeek(result.getAsJsonArray("players").get(i).getAsJsonObject().get("weeklyLosses").getAsInt());
                        player.setRating(result.getAsJsonArray("players").get(i).getAsJsonObject().get("rating").getAsInt());
                        player.setRegionId(result.getAsJsonArray("players").get(i).getAsJsonObject().get("regionId").getAsInt());
                        players.getPlayers().add(player);
                    }
                    realm.beginTransaction();
                    realm.copyToRealmOrUpdate(players);
                    realm.commitTransaction();
                    updateRecycler(players);
                } else if (e != null) {
                    Toast.makeText(getContext(), e.toString(), Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "дичь какая-то", Toast.LENGTH_SHORT).show();
                }
            }
        };
        //endregion

        //region Request
        switch (ladderId){
            case Players.LADDER_3V3 :
                request = new RequestBuilder(getContext())
                        .withURL("https://warcraftassistant.azurewebsites.net/apiv1/getLadder3v3")
                        .withCallback(callback)
                        .build();
                break;
            case Players.LADDER_2V2 :
                request = new RequestBuilder(getContext())
                        .withURL("https://warcraftassistant.azurewebsites.net/apiv1/getLadder2v2")
                        .withCallback(callback)
                        .build();
                break;
            case Players.LADDER_RBG :
                request = new RequestBuilder(getContext())
                        .withURL("https://warcraftassistant.azurewebsites.net/apiv1/getLadderRBG")
                        .withCallback(callback)
                        .build();
                break;
        }
        //endregion

        //region Recycler listeners
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0){
                    ((MainActivity)getActivity()).getFilterFab().hide();
                    ((MainActivity)getActivity()).setFabShowed(false);
                }
                else if (dy < 0) {
                    ((MainActivity)getActivity()).getFilterFab().show();
                    ((MainActivity)getActivity()).setFabShowed(true);
                }
            }
        });
        //endregion

        //region Refresh
        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                request.send();
            }
        });
        //endregion
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }

    public void setLadderId(int ladderId){
        this.ladderId = ladderId;
    }

    public void updateRecycler(Players players){
        multiStateView.setViewState(MultiStateView.VIEW_STATE_CONTENT);
        adapter.notifyDataSetChanged();
        adapter = new RecyclerAdapter(players);
        recyclerView.setAdapter(new AlphaInAnimationAdapter(adapter));
    }
}
