package com.badmanners.wowassistant.activities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.badmanners.wowassistant.R;
import com.badmanners.wowassistant.adapters.ScreenSlidePagerAdapter;
import com.badmanners.wowassistant.dialogs.FilterDialog;
import com.mikepenz.aboutlibraries.LibsBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.action_filter)
    FloatingActionButton filterFab;

    @BindView(R.id.drawer_container)
    RelativeLayout drawerContainer;

    @BindView(R.id.container)
    FrameLayout container;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    private PagerAdapter pagerAdapter;

    private Drawer drawer;
    private boolean fabShowed = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        init();

        initListeners();
    }

    private void init() {

        //region Viewpager with tabs init
        pagerAdapter = new ScreenSlidePagerAdapter(getSupportFragmentManager(),this);
        viewPager.setAdapter(pagerAdapter);
        tabLayout.setupWithViewPager(viewPager);
        //endregion

        //region DrawerContainer fix for Kit-Kat
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.KITKAT) {
            CoordinatorLayout.LayoutParams drawerContainerParams = (CoordinatorLayout.LayoutParams) drawerContainer.getLayoutParams();
            drawerContainerParams.setMargins(0, 0, 0, 0);
            drawerContainer.setLayoutParams(drawerContainerParams);
            container.setLayoutParams(drawerContainerParams);
            drawerContainer.requestLayout();
            container.requestLayout();
        }
        //endregion

        //region Drawer init
        drawer = new DrawerBuilder(this)
                .withRootView(R.id.drawer_container)
                .withToolbar(toolbar)
                .withDisplayBelowStatusBar(true)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(new PrimaryDrawerItem().withName(R.string.ladder), new PrimaryDrawerItem().withName(R.string.auction))
                .withOnDrawerListener(new Drawer.OnDrawerListener() {
                    @Override
                    public void onDrawerOpened(View drawerView) {
                    }

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        if (fabShowed){
                            filterFab.show();
                        }
                    }

                    @Override
                    public void onDrawerSlide(View drawerView, float slideOffset) {
                        if (slideOffset>0){
                            filterFab.hide();
                        }
                    }
                })
                .build();
        //endregion

    }

    private void initListeners() {
        //region FilterDialog
        filterFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                FilterDialog filterDialog = new FilterDialog();
                filterDialog.show(getSupportFragmentManager(), "A");
            }
        });
        //endregion
   }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        } else if (id == R.id.action_about) {
            new LibsBuilder()
                    .withAboutIconShown(true)
                    .withVersionShown(true)
                    .withAboutAppName(getString(R.string.app_name))
                    .withSortEnabled(true)
                    .withLicenseShown(true)
                    .withActivityTitle(getString(R.string.action_about))
                    .start(this);
        }

        return super.onOptionsItemSelected(item);
    }

    public FloatingActionButton getFilterFab() {
        return filterFab;
    }

    public void setFabShowed(boolean fabShowed) {
        this.fabShowed = fabShowed;
    }

    public boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnectedOrConnecting();
    }
}
